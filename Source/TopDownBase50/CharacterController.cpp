// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterController.h"
#include "ClasesDT.h"
#include "En.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

// ACharacterController::ACharacterController() { }
//
// void ACharacterController::BeginPlay() {
// 	Super::BeginPlay();
// }
//
// void ACharacterController::PlayerTick(float DeltaTime) {
// 	Super::PlayerTick(DeltaTime);
// }
//
//
// void ACharacterController::SetupInputComponent() {
// 	Super::SetupInputComponent();
//
// 	InputComponent->BindAction("Golpea", IE_Pressed, this, &ACharacterController::HitAll);
// 	InputComponent->BindAction("GetClass", IE_Pressed, this, &ACharacterController::GetClass);
// }
//
// void ACharacterController::HitAll() {
// 	evOnHitAll.Broadcast();
//
// 	TArray<AActor*> OutAct;
// 	UGameplayStatics::GetAllActorsWithInterface(this, UHittable::StaticClass(), OutAct);
//
// 	for (AActor* act : OutAct) {
// 		IHittable* hitto = Cast<IHittable>(act);
// 		if(hitto) {
// 			hitto->Hit_Implementation(2);
// 			UE_LOG(LogTemp, Warning, TEXT("Hitto %d/%d"), hitto->hp, hitto->initialHp);
// 		}
// 	}
// }
//
// void ACharacterController::GetClass() {
// 	if(ClassDB && !miClass && !className.IsNone()){
// 		static const FString context = FString("Obtener clase con la C");
// 		FClasesDT* clase = ClassDB->FindRow<FClasesDT>(className, context, false);
// 		if(clase)
// 			miClass = clase;		
// 	}else {
// 		if(miClass) {
// 			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
// 		}
// 	}
// }
