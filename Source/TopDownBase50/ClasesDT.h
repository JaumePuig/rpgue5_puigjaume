// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ClasesDT.generated.h"

UENUM(BlueprintType)
enum EClases {
	MAGO,
	GUERRERO
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FClasesDT : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int damageModifier;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName skill1;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName skill2;

	FClasesDT() : myclass(MAGO), initialHp(100), damageModifier(1), skill1("Fire Missile"), skill2("Fire Cloud") {}
	
};
