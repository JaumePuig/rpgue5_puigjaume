// Fill out your copyright notice in the Description page of Project Settings.


#include "En.h"

#include "TopDownBase50Character.h"
#include "TopDownBase50GameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"

// Sets default values
AEn::AEn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	sightConf = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConf"));
	sightConf->SightRadius = BIG_NUMBER;
	sightConf->LoseSightRadius = BIG_NUMBER;
	sightConf->PeripheralVisionAngleDegrees = 360.f;
	sightConf->DetectionByAffiliation.bDetectEnemies = true;
	sightConf->DetectionByAffiliation.bDetectFriendlies = true;
	sightConf->DetectionByAffiliation.bDetectNeutrals = true;
	sightConf->SetMaxAge(.1);

	AIPerception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerception->ConfigureSense(*sightConf);
	AIPerception->SetDominantSense(sightConf->GetSenseImplementation());
	AIPerception->OnPerceptionUpdated.AddDynamic(this, &AEn::OnSensed);

	currVel = FVector::ZeroVector;
	moveSpeed = 5.f;
	distanceSQRT = BIG_NUMBER;
	
}

// Called when the game starts or when spawned
void AEn::BeginPlay()
{
	Super::BeginPlay();

	basePos = GetActorLocation();
	
}

void AEn::Hit_Implementation(float dmg) {
	IHittable::Hit_Implementation(dmg);

	hp -= dmg;

	if(hp <= 0)
	{
		Die_Implementation();
	}
	
	// UE_LOG(LogTemp, Warning, TEXT("Manpegao %s %d/%d"), *this->GetName(), hp, initialHp);
}

void AEn::Die_Implementation() {
	IHittable::Die_Implementation();

	Cast<ATopDownBase50GameMode>(UGameplayStatics::GetGameMode(GetWorld()))->GiveExperience();
	
	Destroy();
	
	// UE_LOG(LogTemp, Warning, TEXT("Manpegao %s %d/%d"), *this->GetName(), hp, initialHp);
}

// Called every frame
void AEn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!currVel.IsZero()) {
		newPos = GetActorLocation() + currVel + DeltaTime;

		if (backToBase) {
			double sqrt = (newPos - basePos).SizeSquared2D();
			if (sqrt < distanceSQRT) {
				distanceSQRT = sqrt;
			}
			else {
				currVel = FVector::ZeroVector;
				distanceSQRT = BIG_NUMBER;
				backToBase = false;

				SetNewRotation(GetActorForwardVector(), GetActorLocation());
			}
		}

		SetActorLocation(newPos);
	}
	
}

void AEn::OnSensed(const TArray<AActor*>& upActors)
{
	UE_LOG(LogTemp, Warning, TEXT("on sensed"));
	for (const auto& act : upActors) {

		ATopDownBase50Character* player = Cast<ATopDownBase50Character>(act);

		if (player) {
			UE_LOG(LogTemp, Warning, TEXT("jugador detectado"));

			FActorPerceptionBlueprintInfo info;
			AIPerception->GetActorsPerception(act, info);

			FVector dir;
			if (info.LastSensedStimuli[0].WasSuccessfullySensed()) {
				dir = act->GetActorLocation() - GetActorLocation();
				dir.Z = 0;

				currVel = dir.GetSafeNormal() * moveSpeed;

				SetNewRotation(act->GetActorLocation(), GetActorLocation());
			}

		}

	}
	
}

void AEn::SetNewRotation(FVector target, FVector curr) {
	FVector newDir = target - curr;
	newDir.Z = 0.f;

	enemyRot = newDir.Rotation();

	SetActorRotation(enemyRot);
}

// Called to bind functionality to input
void AEn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

