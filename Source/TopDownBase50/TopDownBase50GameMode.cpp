// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownBase50GameMode.h"
#include "TopDownBase50PlayerController.h"
#include "TopDownBase50Character.h"
#include "GameFramework/GameSession.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

ATopDownBase50GameMode::ATopDownBase50GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATopDownBase50PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
	
}

void ATopDownBase50GameMode::GiveExperience()
{

	player->exp++;

	if (player->exp >= player->neededExp)
	{
		player->LevelUp();
	}
	
}

void ATopDownBase50GameMode::BeginPlay()
{
	Super::BeginPlay();

	//player = Cast<ATopDownBase50PlayerController>(UGameplayStatics::GetGameMode(GetWorld()));
	player = Cast<ATopDownBase50PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	
}
