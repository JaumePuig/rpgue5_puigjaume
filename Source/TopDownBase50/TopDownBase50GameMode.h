// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "TopDownBase50PlayerController.h"
#include "GameFramework/GameModeBase.h"
#include "TopDownBase50GameMode.generated.h"

UCLASS(minimalapi)
class ATopDownBase50GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere)
	ATopDownBase50PlayerController* player;
	
	ATopDownBase50GameMode();
	void GiveExperience();

protected:
	virtual void BeginPlay() override;
};



