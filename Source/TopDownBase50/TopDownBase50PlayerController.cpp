// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownBase50PlayerController.h"

#include <string>

#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "TopDownBase50Character.h"
#include "Engine/World.h"
#include "ClasesDT.h"
#include "En.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

ATopDownBase50PlayerController::ATopDownBase50PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}

void ATopDownBase50PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	if(bInputPressed)
	{
		FollowTime += DeltaTime;

		// Look for the touch location
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		if(bIsTouch)
		{
			GetHitResultUnderFinger(ETouchIndex::Touch1, ECC_Visibility, true, Hit);
		}
		else
		{
			GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		}
		HitLocation = Hit.Location;

		// Direct the Pawn towards that location
		APawn* const MyPawn = GetPawn();
		if(MyPawn)
		{
			FVector WorldDirection = (HitLocation - MyPawn->GetActorLocation()).GetSafeNormal();
			MyPawn->AddMovementInput(WorldDirection, 1.f, false);
		}
	}
	else
	{
		FollowTime = 0.f;
	}
}

void ATopDownBase50PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATopDownBase50PlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ATopDownBase50PlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATopDownBase50PlayerController::OnTouchPressed);
	InputComponent->BindTouch(EInputEvent::IE_Released, this, &ATopDownBase50PlayerController::OnTouchReleased);

	InputComponent->BindAction("Golpea", IE_Pressed, this, &ATopDownBase50PlayerController::HitAll);
	InputComponent->BindAction("GetClass", IE_Pressed, this, &ATopDownBase50PlayerController::GetClass);
	InputComponent->BindAction("Habilidad1", IE_Released, this, &ATopDownBase50PlayerController::UseFirstAbility);
	InputComponent->BindAction("Habilidad2", IE_Released, this, &ATopDownBase50PlayerController::UseSecondAbility);

}

void ATopDownBase50PlayerController::OnSetDestinationPressed()
{
	// We flag that the input is being pressed
	bInputPressed = true;
	// Just in case the character was moving because of a previous short press we stop it
	StopMovement();
}

void ATopDownBase50PlayerController::OnSetDestinationReleased()
{
	// Player is no longer pressing the input
	bInputPressed = false;

	// If it was a short press
	if(FollowTime <= ShortPressThreshold)
	{
		// We look for the location in the world where the player has pressed the input
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		HitLocation = Hit.Location;

		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, HitLocation);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, HitLocation, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void ATopDownBase50PlayerController::OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = true;
	OnSetDestinationPressed();
}

void ATopDownBase50PlayerController::OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ATopDownBase50PlayerController::HitAll() {
	evOnHitAll.Broadcast();

	TArray<AActor*> OutAct;
	UGameplayStatics::GetAllActorsWithInterface(this, UHittable::StaticClass(), OutAct);

	for (AActor* act : OutAct) {
		IHittable* hitto = Cast<IHittable>(act);
		if(hitto) {
			hitto->Hit_Implementation(2);
			UE_LOG(LogTemp, Warning, TEXT("Hitto %d/%d"), hitto->hp, hitto->initialHp);
		}
	}
}

void ATopDownBase50PlayerController::GetClass() {
	if(ClassDB && !miClass && !className.IsNone()){
		static const FString context = FString("Obtener clase con la C");
		FClasesDT* clase = ClassDB->FindRow<FClasesDT>(className, context, false);
		if(clase)
			miClass = clase;		
	}else {
		if(miClass) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
		}
	}
}

void ATopDownBase50PlayerController::GetInitialStats()
{

	if(ClassDB && !miClass && !className.IsNone()){
		static const FString context = FString("Obtener estadisticas iniciales");
		FClasesDT* clase = ClassDB->FindRow<FClasesDT>(className, context, false);
		if(clase)
		{

			miClass = clase;
			level = 1;
			exp = 0;
			neededExp = 5;
			maxHp = clase->initialHp;
			currentHp = maxHp;
			damageModifier = 1.f;
			skill1 = clase->skill1;
			skill2 = clase->skill2;
			
		}
	}else {
		if(miClass) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
		}
	}
	
}

void ATopDownBase50PlayerController::UseFirstAbility()
{
	const FSkillsDT* spellALanzar = GetSkill(skill1);
	UE_LOG(LogTemp, Warning, TEXT("lanzar primera habilidad 1"));
	FVector HitLocation = FVector::ZeroVector;
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	bool bIsHittable = false;
	if (Hit.GetActor())
	{
		bIsHittable = UKismetSystemLibrary::DoesImplementInterface(
			Hit.GetActor(), UHittable::StaticClass());
	}
	
	if (spellALanzar)
	{
		
		switch (spellALanzar->castType)
		{
			case OnDirection:
				UE_LOG(LogTemp, Warning, TEXT("lanzar primera habilidad 3"),);
				evOnDirectionSkillUsed.Broadcast((Hit.Location - GetPawn()->GetActorLocation()).Rotation(), *spellALanzar, spellALanzar->baseDamage);
				break;
			case OnLocation:
				evOnLocationSkillUsed.Broadcast(Hit.Location, *spellALanzar, spellALanzar->baseDamage);
				break;
		}
		
	}
	
}

void ATopDownBase50PlayerController::UseSecondAbility()
{
	const FSkillsDT* spellALanzar = GetSkill(skill2);
	UE_LOG(LogTemp, Warning, TEXT("lanzar segunda habilidad 1"));
	FVector HitLocation = FVector::ZeroVector;
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_Visibility, true, Hit);
	bool bIsHittable = false;
	if (Hit.GetActor())
	{
		bIsHittable = UKismetSystemLibrary::DoesImplementInterface(
			Hit.GetActor(), UHittable::StaticClass());
	}
	
	if (spellALanzar)
	{
		
		switch (spellALanzar->castType)
		{
		case OnDirection:
			
			evOnDirectionSkillUsed.Broadcast((Hit.Location - GetPawn()->GetActorLocation()).Rotation(), *spellALanzar, spellALanzar->baseDamage);
			break;
		case OnLocation:
			UE_LOG(LogTemp, Warning, TEXT("lanzar segunda habilidad 2"));
			evOnLocationSkillUsed.Broadcast(Hit.Location, *spellALanzar, spellALanzar->baseDamage);
			break;
		}
		
	}
	
}

void ATopDownBase50PlayerController::LevelUp()
{

	maxHp += 10;
	currentHp = maxHp;
	exp = 0;
	neededExp += 5;
	level++;
	damageModifier += 0.1f;
	
}

const FSkillsDT* ATopDownBase50PlayerController::GetSkill(FName skillName)
{
	if (skillDB)
	{
		
		static const FString context = FString("Trying to find ").Append(skillName.ToString());
		FSkillsDT* skill = skillDB->FindRow<FSkillsDT>
		(skillName, context, false);
		return skill;
	}
	return nullptr;
}
