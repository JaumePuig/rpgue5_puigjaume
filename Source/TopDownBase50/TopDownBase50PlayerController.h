// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "ClasesDT.h"
#include "SkillsDT.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "TopDownBase50PlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHitAll);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTargetSkillUsed, const FSkillsDT, spell);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnLocationSkillUsed, FVector, target, const FSkillsDT, spell, int, damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDirectionSkillUsed, FRotator, target, const FSkillsDT, spell, int, damage);

/** Forward declaration to improve compiling times */
class UNiagaraSystem;

UCLASS()
class ATopDownBase50PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownBase50PlayerController();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	//Cosas no base
	UPROPERTY(BlueprintAssignable)
	FOnHitAll evOnHitAll;

	UPROPERTY(BlueprintAssignable)
	FOnTargetSkillUsed evOnTargetSkillUsed;
	UPROPERTY(BlueprintAssignable)
	FOnLocationSkillUsed evOnLocationSkillUsed;
	UPROPERTY(BlueprintAssignable)
	FOnDirectionSkillUsed evOnDirectionSkillUsed;

	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* ClassDB;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName className;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int level;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int exp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int neededExp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int maxHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int currentHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float damageModifier;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName skill1;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName skill2;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	UDataTable* skillDB;
	
	FClasesDT* miClass;

	//virtual void BeginPlay() override;
	void HitAll();
	void GetClass();
	UFUNCTION(BlueprintCallable)
	void GetInitialStats();
	UFUNCTION(BlueprintCallable)
	void UseFirstAbility();
	UFUNCTION(BlueprintCallable)
	void UseSecondAbility();
	void LevelUp();
	
	const FSkillsDT* GetSkill(FName skillName);

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location);

private:
	bool bInputPressed; // Input is bring pressed
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


